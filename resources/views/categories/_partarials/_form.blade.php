
<form method="POST" action="{{route('categories.store')}}">
    @csrf
    <div>
        <label>{{trans('Общество')}}
            <input name="society">
            {!! $errors->first('society','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('День города')}}
            <input name="city_day">
            {!! $errors->first('city_day','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('Спорт')}}
            <input name="sport">
            {!! $errors->first('sport','<span >:message</span>') !!}
        </label>
    </div>
    <button type="submit">Добавить</button>
</form>
