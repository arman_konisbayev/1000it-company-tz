
<form method="POST" action="{{route('news.store')}}">
    @csrf
    <div>
        <label>{{trans('label.title')}}
            <input name="title">
            {!! $errors->first('title','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('label.preview')}}
            <input name="preview">
            {!! $errors->first('preview','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('label.text')}}
            <input name="text">
            {!! $errors->first('text','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('label.date_publicate')}}
            <input name="date_publicate">
            {!! $errors->first('date_publicate','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('Рубрика')}}
            <input name="category">
            {!! $errors->first('category','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('Автор')}}
            <input name="author">
            {!! $errors->first('author','<span >:message</span>') !!}
        </label>
    </div>
    <button type="submit">Добавить</button>
</form>
