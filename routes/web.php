<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'MainController@index')->name('index');

//Route::get('/', 'UsersController@index');


Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news/create', 'NewsController@create')->name('news.create')->middleware('localization');;
Route::post('/news/store', 'NewsController@store')->name('news.store')->middleware('localization');
Route::get('/authors/create', 'AuthorsController@create')->name('authors.create')->middleware('localization');;
Route::post('/authors/store', 'AuthorsController@store')->name('authors.store')->middleware('localization');
Route::get('/categories/create', 'CategoriesController@create')->name('categories.create')->middleware('localization');;
Route::post('/categories/store', 'CategoriesController@store')->name('categories.store')->middleware('localization');
