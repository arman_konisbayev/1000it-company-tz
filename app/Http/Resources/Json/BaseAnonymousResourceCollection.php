<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Pagination\AbstractPaginator;

class BaseAnonymousResourceCollection extends AnonymousResourceCollection
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        return $this->resource instanceof AbstractPaginator
            ? (new BasePaginatedResourceResponse($this))->toResponse($request)
            : parent::toResponse($request);
    }
}
