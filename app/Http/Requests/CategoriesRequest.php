<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $rules = [
            'society' => 'required|string',
            'city_day' => 'required|string',
            'sport' => 'required|string',
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'society'  => trans('Общество'),
            'city_day'  => trans('День города'),
            'sport'  => trans('Спорт'),
        ];
    }
}
