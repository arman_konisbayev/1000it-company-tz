<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorsRequest;
use App\Models\Author;

class AuthorsController extends Controller
{
    public function create()
    {
        return view(
            'authors.create'
        );
    }

    public function store(AuthorsRequest $request)
    {
        $data = $request->all();
        Author::create($data);
    }
}
