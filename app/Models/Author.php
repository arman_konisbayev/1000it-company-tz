<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int                $id
 * @property string             $fio
 * @property string             $email
 * @property string             $avatar
 * @property Carbon             $created_at
 * @property Carbon             $updated_at
 * @mixin Eloquent
 */
class Author extends Model
{
    protected $table = 'authors';

    protected $fillable = [
        'fio',
        'email',
        'avatar',
    ];
}
